//ClienteService

package br.com.mastertech.revisao.revisao.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.mastertech.revisao.revisao.model.Cliente;
import br.com.mastertech.revisao.revisao.repository.ClienteRepository;


@Service
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;

	public Iterable<Cliente> obterCliente(){
		System.out.println("Chamaram o listar clientes");
		return clienteRepository.findAll();
	}

	public Cliente obterClientePeloID(Long id) {
		return verificarExisteCliente(id);

	}
	

	
	public Cliente criarCliente(Cliente cliente) {
		clienteRepository.save(cliente);
		
		return obterClientePeloID(cliente.getId());
		//System.out.println("Chamaram o criar do " + cliente.getNome());
		
		//Obter o Cliente que foi salvo
		
		
	}

	
	//Verificar se cliente existe
	public Cliente verificarExisteCliente(Long id) {
		Optional<Cliente> optional = clienteRepository.findById(id);

		// Caso o cliente não seja encontrado, devolve uma exceção.
		if (!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "cliente não encontrado.");
		}

		// Caso tenha encontrado, devolve o cliente.
		return optional.get();
	}

	//Editar cliente
	public Cliente editarcliente(Long id, Cliente cliente) {
		//Verifica se o cliente existe.
		Cliente clienteEncontrado = verificarExisteCliente(id);

		//Informar campos que serão editados.
		clienteEncontrado.setNome(cliente.getNome());

		//Salvar alterações
		return clienteRepository.save(clienteEncontrado);
	}

	//Excluir cliente
	public void apagarcliente(Long id) {
		Cliente cliente = verificarExisteCliente(id);
		clienteRepository.delete(cliente);
	}

}
