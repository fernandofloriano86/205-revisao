package br.com.mastertech.revisao.revisao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.mastertech.revisao.revisao.model.Produto;
import br.com.mastertech.revisao.revisao.service.ProdutoService;

@RestController
public class ProdutoController {

	@Autowired
	private ProdutoService produtoService;
	
	//GET - Obtém lista de produtos cadastrados
	@GetMapping("/produto")
	public Iterable<Produto> listarProdutos() {
		return produtoService.obterProdutos();
	}
	
	//POST - Cria um produto no sistema de acordo com o nome e rendimento enviados.
	@PostMapping("/produto")
	@ResponseStatus(code = HttpStatus.OK)
	public Produto criarFilme(@RequestBody Produto produto) {
		return produtoService.criarProduto(produto);
	}
	
	//PUT - Edita um produto do sistema (PUT /produto/{id})
	@PutMapping("/produto/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public Produto editarProduto(@PathVariable Long id, @RequestBody Produto produto) {
		return produtoService.editarProduto(id, produto);
	}
	
	//DELETE - Apaga um produto do sistema.
	@DeleteMapping("/produto/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void apagarProduto(@PathVariable Long id) {
		produtoService.apagarProduto(id);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}