package br.com.mastertech.revisao.revisao.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.mastertech.revisao.revisao.model.Aplicacao;
import br.com.mastertech.revisao.revisao.model.Cliente;
import br.com.mastertech.revisao.revisao.repository.AplicacaoRepository;

@Service
public class AplicacaoService {

	@Autowired
	private AplicacaoRepository aplicacaoRepository;
	
	@Autowired
	private ClienteService clienteService;
	

	// Obter a lista de todos os produtos cadastrados.
	public Iterable<Aplicacao> obterAplicacoes() {
		return aplicacaoRepository.findAll();
	}

	// Verificar se produto existe
	public Aplicacao verificarExisteAplicacao(Long id) {
		Optional<Aplicacao> optional = aplicacaoRepository.findById(id);

		// Caso o produto não seja encontrado, devolve uma exceção.
		if (!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Produto não encontrado.");
		}

		// Caso tenha encontrado, devolve o produto.
		return optional.get();
	}
	
	public Aplicacao obterAplicacaoPorId(Long id) {
		return verificarExisteAplicacao(id);
	}
	
	public Aplicacao incluirAplicacao(Long idCLiente, Aplicacao aplicacao) {
		
		Cliente cliente = clienteService.obterClientePeloID(idCLiente);
		
		cliente.set
		return aplicacaoRepository.save(aplicacao);
	}
	
}