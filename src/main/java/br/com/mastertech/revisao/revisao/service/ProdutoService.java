package br.com.mastertech.revisao.revisao.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import br.com.mastertech.revisao.revisao.model.Produto;
import br.com.mastertech.revisao.revisao.repository.ProdutoRepository;

@Service
public class ProdutoService {

	@Autowired
	private ProdutoRepository produtoRepository;
	
	//Obter a lista de todos os produtos cadastrados.
	public Iterable<Produto> obterProdutos() {
		return produtoRepository.findAll();
	}
	
	private Produto obterProdutoPorId(Long id) {
		return verificarExisteProduto(id);
	}
	
	//Cadastrar novo produto
	public Produto criarProduto(Produto produto) {
		produtoRepository.save(produto);
		
		//Obter dados do cliente que foi salvo
		return produto;
	}
	
	//Verificar se produto existe
	public Produto verificarExisteProduto(Long id) {
		Optional<Produto> optional = produtoRepository.findById(id);

		// Caso o produto não seja encontrado, devolve uma exceção.
		if (!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Produto não encontrado.");
		}
		
		// Caso tenha encontrado, devolve o produto.
		return optional.get();
	}
	
	//Editar produto
	public Produto editarProduto(Long id, Produto produto) {
		//Verifica se o produto existe.
		Produto produtoEncontrado = verificarExisteProduto(id);
		
		//Informar campos que serão editados.
		produtoEncontrado.setNome(produto.getNome());
		produtoEncontrado.setRendimento(produto.getRendimento());
		
		//Salvar alterações
		return produtoRepository.save(produtoEncontrado);
	}
	
	//Excluir produto
	public void apagarProduto(Long id) {
		Produto produto = verificarExisteProduto(id);
		produtoRepository.delete(produto);
	}
}