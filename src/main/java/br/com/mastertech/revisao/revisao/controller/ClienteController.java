// Cliente Controller

package br.com.mastertech.revisao.revisao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.mastertech.revisao.revisao.model.Aplicacao;
import br.com.mastertech.revisao.revisao.model.Cliente;
import br.com.mastertech.revisao.revisao.service.ClienteService;

@RestController
public class ClienteController {

	@Autowired
	private ClienteService clienteService;

	@GetMapping("/cliente")
	public Iterable<Cliente> obterCliente() {
		return clienteService.obterCliente();
	}

	@PostMapping("/cliente")
	@ResponseStatus(code = HttpStatus.OK)
	public Cliente criarCliente(@RequestBody Cliente cliente) {
		return clienteService.criarCliente(cliente);
	}

	@PutMapping("/cliente/{id}")
	public Cliente editarCliente(@PathVariable Long id, @RequestBody Cliente cliente) {
		return clienteService.editarcliente(id, cliente);
	}

	@DeleteMapping("/cliente/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void apagarCliente(@PathVariable Long id) {
		clienteService.apagarcliente(id);
	}
	



}
