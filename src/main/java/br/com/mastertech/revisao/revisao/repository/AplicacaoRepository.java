package br.com.mastertech.revisao.revisao.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.mastertech.revisao.revisao.model.Aplicacao;

@Repository
public interface AplicacaoRepository extends CrudRepository<Aplicacao, Long>{

}