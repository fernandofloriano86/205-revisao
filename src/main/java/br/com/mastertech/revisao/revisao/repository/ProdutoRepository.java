package br.com.mastertech.revisao.revisao.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.mastertech.revisao.revisao.model.Produto;

@Repository
public interface ProdutoRepository extends CrudRepository<Produto, Long> {

}