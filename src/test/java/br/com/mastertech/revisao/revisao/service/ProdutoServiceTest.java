package br.com.mastertech.revisao.revisao.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import br.com.mastertech.revisao.revisao.model.Produto;
import br.com.mastertech.revisao.revisao.repository.ProdutoRepository;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ProdutoService.class})
public class ProdutoServiceTest {

	@Autowired
	ProdutoService produtoService;
	
	@MockBean
	ProdutoRepository produtoRepository;

	@Test
	public void deveListarTodosOsProdutos() {
		
		//setup
		Produto produto = new Produto();
		List<Produto> produtos = new ArrayList<>();
		produtos.add(produto);
		
		Mockito.when(produtoRepository.findAll()).thenReturn(produtos);
		
		//action
		List<Produto> resultado = Lists.newArrayList(produtoService.obterProdutos());
		
		//check
		Produto produtoResultado = resultado.get(0);
		
		assertNotNull(produtoResultado);
		assertEquals(produto, resultado.get(0));
	}
	
	@Test
	public void deveIncluirUmProduto() {
		
		//setup
		Produto produto = new Produto();
		produto.setNome("PIC");
		produto.setRendimento(20);
		
		//action
		produtoService.criarProduto(produto);
				
		//check
		Mockito.verify(produtoRepository).save(produto);
	}
	
	@Test
	public void deveExcluirUmProduto() {
		//setup
		long id = 1;
		Produto produto = new Produto();
		produto.setId(id);
		produto.setNome("PIC");
		produto.setRendimento(40);
		
		Mockito.when(produtoRepository.findById(id)).thenReturn(Optional.of(produto));
		
		//action
		produtoService.apagarProduto(id);
		
		//check
		Mockito.verify(produtoRepository).delete(produto);
		
	}
	
	@Test(expected = ResponseStatusException.class)
	public void deveLancarExcecaoQuandoNaoEncontrarOProduto() {
		//setup
		long id = 1;
		Produto produto = new Produto();
		produto.setId(id);
		produto.setNome("Diretora Maria");
		produto.setRendimento(40);
		
		Mockito.when(produtoRepository.findById(id)).thenReturn(Optional.empty());
		
		//action
		produtoService.apagarProduto(id);
	}
	
	
}
